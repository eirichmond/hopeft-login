<?php

/**
 * Fired during plugin activation
 *
 * @link       http://squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Hopeft_Login
 * @subpackage Hopeft_Login/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Hopeft_Login
 * @subpackage Hopeft_Login/includes
 * @author     Your Name <email@example.com>
 */
class Hopeft_Login_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
